# Build Simple Website with NodeJS, Express & EJS view engine

## Document

* Install dependencies using [npm](https://www.npmjs.com/) javascript package manager: ``` npm install ```
* Start node server: ``` npm start ```
* Tune to url: ``` http://localhost:3000 ```

